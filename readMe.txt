Okay so, I was unable to deal with the punctuation in the Marklar part of the 
assignment. I uploaded a java program I wrote last summer in anticipation of 
having to do something similar in my coursework. It reads a textfile and 
reports how often each word occurs. It splits the text up at spaces, and 
eliminates leading and trailing punctuation (using regex no less!) and leaves 
the inner punctuation alone. With enough time and research I know that I could 
figure out how to make it work in ruby, but I want to submit this and make
contact with Jaguar Studios with as little delay as possible.


About wordSort:

*the main method is in wordSort.java

*it requires three files (wordSort.java, numberSort.java, and fileReader.java)
 in order to run

*when it asks the user for a filename, it is up to the user to specify an 
 extension, however when it asks for the output file name to save the results
 as, no extension is necessarry as .txt is automatically appended.

*good text files for testing can be found here: 
      
      http://introcs.cs.princeton.edu/java/data/



--Kelly Clark
