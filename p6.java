/*
*Kelly Clark
*KCLARK99@MAIL.COM
*p6.java
*Six degrees of Bacon
*cs20j assignment no.6
*version 1.00
*working, tested, compiles, runs
*missing docs, not fully complete but functionally complete.
*extra credit not completed.
*
*/
import java.io.*;
import java.util.*;


public class p6{
	public static ArrayList<WNode> toCheckAgainst = new ArrayList<WNode>();
	public static ArrayList<ArrayList<WNode>> compoundList = new ArrayList<ArrayList<WNode>>();
	public ArrayList<WNode> tempList = new ArrayList<WNode>();
	public static int numTimes = 1;
	public boolean foundBacon = false;
	public String uInput;
	public static Scanner aScan = new Scanner(System.in);
	public static Scanner userIn = new Scanner(System.in);
	protected WNode first;
	protected WNode userNode;
	protected class WNode{
	protected WNode pointer;
	protected String data;
	public WNode(){ this.data = null; this.pointer = null;}
	}	

	public p6(){
	userNode = new WNode();
	first = new WNode();
	}


/*
*.run() is the main program loop
*
*/

 	public void run(){
		ArrayList<WNode> aList = toArrList();
		ArrayList<WNode> firstOne = new ArrayList<WNode>();
		compoundList.add(aList);
		uInput = getUserInput();
		userNode.data = uInput;
		firstOne.add(userNode);
		tempList = firstOne;
		compoundList.add(firstOne);
		while(!foundBacon){
			rinseRepeat(compoundList.get(numTimes), compoundList.get(0));
				numTimes++;
		}
 	} 	
	
	/*
	*heres my main method
	*it takes the filename of the word list either from a command-line argumennt or a string enterd by the user if no argument is given
	*@param args is command line arguments
	*/
	public static void main (String[] args){
		p6 ding = new p6();
		if(args.length>0){
			try{
				userIn = new Scanner(new BufferedReader(new FileReader(args[0])));
				ding.getWords(args[0]);
			}catch(Exception e){
				System.out.println("file not found"); return;
			}
		}else{userIn = new Scanner(System.in);
		 System.out.println("enter a filename");
		 try{
		 String fileNameStr = aScan.nextLine();
		 userIn = new Scanner(new BufferedReader(new FileReader(fileNameStr)));
				ding.getWords(fileNameStr);
			}catch(Exception e){
				System.out.println("file not found"); return;
			}
		}
		ding.run(); 
	}

	/*
	* alreadyHas checks if the word that is one off  of the word that is being tested,
	*is already in the list of words that have been used, and returns true/false accordingly
	*/
	public static boolean alreadyHas(String wordCheck){
		boolean toReturn = false;
		for(int i = 0; i < toCheckAgainst.size(); i++){
			if(wordCheck.equals(toCheckAgainst.get(i).data)){
				toReturn = true;
			}
		}
		return toReturn;
	}



	/*
	*
	*this method runs over and over until bacon is found, adding already used words in the path to bacon to a list
	*that it compares to, in order to ensure the program wont lock up in a loop of alternating between the same two words that are one letter off
	*
	*it also maintains a linked list, with the last node being the node containing the user's entry, and the rest of the nodes rapidly changing to maintain the 
	*connection between the user's node, and whatever word is being tested. 
	*once it finds bacon it stops searching immediately and prints the path back to the user node.
	*
	*@param closenodes is the most recent iteration of nodes whose strings are one off from the previous iteration.
	*@param dict is the dictionary containing all of the owrds.
	*/

	public void rinseRepeat(ArrayList<WNode> closeNodes, ArrayList<WNode> dict){
		ArrayList<WNode> offOne = new ArrayList<WNode>();
		String use;
		for(int j = 0; j<dict.size();j++){
			use = dict.get(j).data; 
			for(int i = 0; i <closeNodes.size();i++){
				String checkThis = closeNodes.get(i).data;
				int numberMatching = 0;
				for(int k = 0; k < checkThis.length(); k++){
					if(use.charAt(k)==checkThis.charAt(k)){
					numberMatching++;
					}
				}
				if (numberMatching==4) {
					if(!alreadyHas(dict.get(j).data)){
						offOne.add(dict.get(j));
						offOne.get(offOne.size()-1).pointer = closeNodes.get(i);
						toCheckAgainst.add(dict.get(j));
						if(dict.get(j).data.equals("bacon")){
							first = dict.get(j);
							foundBacon = true; System.out.println();
							WNode tpain = dict.get(j);
							printPath(tpain);
							return;
						}
					}									
				}
			}
		}
		tempList = offOne;
		compoundList.add(tempList);
	}

	public void getWords(String inFile){	
		while(userIn.hasNextLine()){addToList(userIn.nextLine());}
	}

	public boolean chkStr(String B){
		boolean isInDict = false;
		String A = B;
			if((A.length()>5) || (A.length()<5)){
				System.out.println("not a valid entry");
				return false;
			}else{
				for(int i = 0; i< compoundList.get(0).size(); i++){
					if(A.equals(compoundList.get(0).get(i).data)){isInDict=true;}
				}
			}	
		return isInDict;
	}

	public void addToList(String usrStr){
		WNode temp = new WNode();
		temp.data = usrStr;
		temp.pointer = first;
		first = temp;
	}
/*
*toArrList disassembles a linked list, storing each node in an array list slot.
*/
	public ArrayList<WNode> toArrList(){
		ArrayList<WNode> abc = new ArrayList<WNode>();
		WNode temp = first;
		while(temp.data!= null){
			abc.add(temp); 
			temp = temp.pointer;
		}
		return abc;
	}
/*getUserInput
*collects a five letter word from the user, checks it for validity
*/
	public String getUserInput(){
		userIn = new Scanner(System.in);
		System.out.println("Please enter a five letter word: ");
		String toBeChecked = userIn.nextLine();
			while(!chkStr(toBeChecked)){
				System.out.println("Please enter a five letter word: ");
				toBeChecked = userIn.nextLine();
			}
		String toBeReturned = toBeChecked;
		return toBeReturned;
	}

/*
*prints the path form bacon to the user string
*/
	public static void printPath(WNode bacon){
		WNode tpain = bacon;
		while(tpain.data != null){
			System.out.println(tpain.data);	
			if(tpain.pointer!=null){
			tpain = tpain.pointer;
			}else{return;}
		}
	}
	//***INCOMPLETE EXTRA CREDIT
	public static void printRPath(WNode bacon){
		ArrayList<WNode> backwardsList = new ArrayList<WNode>();

		WNode tpain = bacon;
			while(tpain.data != null){
			backwardsList.add(tpain);	
			if(tpain.pointer!=null){
			tpain = tpain.pointer;
			}else{return;}

			for(int i = 0; i<backwardsList.size(); i++){

				backwardsList.get(i).pointer = backwardsList.get(i+1);
			}

			WNode dang = backwardsList.get(0);
			while(dang.data != null){
			System.out.println(dang.data);
			if(dang.pointer!=null){
			dang = dang.pointer;
			}else{return;}
			}
		}
	}
	//***END OF INCOMPLETE EXTRA CREDIT


}